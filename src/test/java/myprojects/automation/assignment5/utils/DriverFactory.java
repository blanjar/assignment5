package myprojects.automation.assignment5.utils;

import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.net.MalformedURLException;

public class DriverFactory {

    /**
     *
     * @param browser Driver type to use in tests.
     * @return New instance of {@link WebDriver} object.
     */
    public static WebDriver initDriver(String browser) {
        switch (browser) {
            case "firefox":
                System.setProperty(
                        "webdriver.gecko.driver",
                        new File(DriverFactory.class.getResource("/geckodriver.exe").getFile()).getPath());
                return new FirefoxDriver();
            case "ie":
            case "internet explorer":
                System.setProperty(
                        "webdriver.ie.driver",
                        new File(DriverFactory.class.getResource("/IEDriverServer.exe").getFile()).getPath());
                InternetExplorerOptions ieOptions = new InternetExplorerOptions()
                        .destructivelyEnsureCleanSession();
                ieOptions.setCapability(InternetExplorerDriver.NATIVE_EVENTS, false);
                ieOptions.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
                return new InternetExplorerDriver(ieOptions);
            case "android":
                System.setProperty(
                        "webdriver.chrome.driver",
                        new File(DriverFactory.class.getResource("/chromedriver.exe").getFile()).getPath());

                Map<String, String> mobileEmulation = new HashMap<>();
                mobileEmulation.put("deviceName", "Galaxy S5");
                Map<String, Object> mobileEmulationOptions = new HashMap<>();
                mobileEmulationOptions.put("mobileEmulation", mobileEmulation);

                ChromeOptions options = new ChromeOptions();
                options.setCapability(ChromeOptions.CAPABILITY, mobileEmulationOptions);
                return new ChromeDriver(options);
            case "chrome":
            default:
                System.setProperty(
                        "webdriver.chrome.driver",
                        new File(DriverFactory.class.getResource("/chromedriver.exe").getFile()).getPath());
                return new ChromeDriver();
        }
    }

    /**
     *
     * @param browser Remote driver type to use in tests.
     * @param gridUrl URL to Grid.
     * @return New instance of {@link RemoteWebDriver} object.
     */
    public static WebDriver initDriver(String browser, String gridUrl) throws MalformedURLException {
        switch (browser) {
            case "firefox":
                //capabilities = DesiredCapabilities.firefox();
                FirefoxOptions firefoxCapabilities = new FirefoxOptions();
                return new RemoteWebDriver(new URL(gridUrl), firefoxCapabilities);
            case "ie":
            case "internet explorer":
            case "iexplorer":
                InternetExplorerOptions ieCapabilities = new InternetExplorerOptions()
                        .destructivelyEnsureCleanSession();
                ieCapabilities.setCapability(InternetExplorerDriver.NATIVE_EVENTS, false);
                ieCapabilities.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
                return new RemoteWebDriver(new URL(gridUrl), ieCapabilities);
            case "android":
                Map<String, String> mobileEmulation = new HashMap<>();
                mobileEmulation.put("deviceName", "Nexus 5");
                ChromeOptions androidCapabilities = new ChromeOptions();
                androidCapabilities.setExperimentalOption("mobileEmulation", mobileEmulation);
                return new RemoteWebDriver(new URL(gridUrl), androidCapabilities);
            case "chrome":
            default:
                ChromeOptions chromeCapabilities = new ChromeOptions();
                return new RemoteWebDriver(new URL(gridUrl), chromeCapabilities);
        }
    }
}
